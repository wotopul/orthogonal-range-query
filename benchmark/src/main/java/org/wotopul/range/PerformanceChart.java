package org.wotopul.range;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;
import org.openjdk.jmh.results.BenchmarkResult;
import org.openjdk.jmh.results.RunResult;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.wotopul.range.OrthogonalRangeQueryBenchmark.Algorithm;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class PerformanceChart extends Application {
    public static final int DIMENSION = 2;

    private Map<String, Map<Double, Double>> benchmark(String... algorithms) throws RunnerException {
        Options opt = new OptionsBuilder()
            .include(OrthogonalRangeQueryBenchmark.class.getSimpleName())
            .param("algo", algorithms)
            .param("dimension", String.valueOf(DIMENSION))
            .param("nQueries", "100")
            .param("fixedSizeQueries", "true")
            .param("nPoints", nPointsValues())
            .build();

        Collection<RunResult> runResults = new Runner(opt).run();
        Map<String, Map<Double, Double>> result = new HashMap<>();
        for (RunResult runResult : runResults) {
            int nPoints = Integer.parseInt(runResult.getParams().getParam("nPoints"));
            String name = runResult.getParams().getParam("algo");
            Collection<BenchmarkResult> benchmarkResults = runResult.getBenchmarkResults();
            if (benchmarkResults.size() > 1)
                throw new AssertionError();
            double time = benchmarkResults.iterator().next().getPrimaryResult().getScore();
            if (!result.containsKey(name))
                result.put(name, new HashMap<>());
            result.get(name).put((double) nPoints, time);
        }
        return result;
    }

    private String[] nPointsValues() {
        return IntStream.rangeClosed(1, 5).map(x -> x * 10000)
            .mapToObj(String::valueOf).toArray(String[]::new);
    }

    private XYChart.Series<Number, Number> getSeries(String name, Map<Double, Double> chart) {
        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        series.setName(name);
        for (Map.Entry<Double, Double> e : chart.entrySet()) {
            series.getData().add(new XYChart.Data<>(e.getKey(), e.getValue()));
        }
        return series;
    }

    private XYChart.Series[] getSeriesList(Map<String, Map<Double, Double>> chartData) {
        return chartData.entrySet().stream()
            .map(e -> getSeries(e.getKey(), e.getValue()))
            .toArray(XYChart.Series[]::new);
    }

    public void start(Stage stage) {
        Map<String, Map<Double, Double>> chartData;
        try {
            chartData = benchmark(
                Algorithm.LAYERED_RANGE_TREE.toString(),
                Algorithm.RANGE_TREE.toString()
            );
        } catch (RunnerException e) {
            e.printStackTrace();
            return;
        }
        XYChart.Series<Number, Number>[] series = getSeriesList(chartData);
        stage.setTitle("Range-tree performance");
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final ScatterChart<Number,Number> sc = new ScatterChart<>(xAxis,yAxis);
        xAxis.setLabel("nPoints");
        yAxis.setLabel("time");
        sc.setTitle("Parameters");
        sc.getData().addAll(series);
        Scene scene = new Scene(sc, 500, 400);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) throws RunnerException {
        launch(args);
    }
}
