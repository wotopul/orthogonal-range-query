package org.wotopul.range;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.wotopul.range.Util.NonEmptyQuery;

import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class OrthogonalRangeQueryBenchmark {
    public static final double MAX_VALUE = 10;
    public static final double QUERY_DELTA = 0.5;

    public enum Algorithm {
        NAIVE,
        RANGE_TREE,
        LAYERED_RANGE_TREE
    }

    @State(Scope.Thread)
    public static class RandomState {
        @Param({"1", "2", "10"})
        public int dimension;

        @Param({"100"})
        int nPoints;
        Set<Point> points;

        @Param({"RANGE_TREE", "NAIVE"})
        Algorithm algo;
        RangeTree rangeTree;
        LayeredRangeTree layeredRangeTree;

        @Param({"false"})
        boolean fixedSizeQueries;
        @Param({"100"})
        int nQueries;
        List<NonEmptyQuery> queries;

        @Setup(Level.Iteration)
        public void setUp() {
            points = Util.generateRandomPoints(dimension, nPoints, MAX_VALUE);
            if (algo == Algorithm.RANGE_TREE)
                rangeTree = new RangeTree(points);
            else if (algo == Algorithm.LAYERED_RANGE_TREE)
                layeredRangeTree = new LayeredRangeTree(points);
            queries = Util.generateQueries(dimension, nQueries, MAX_VALUE,
                fixedSizeQueries ? QUERY_DELTA : MAX_VALUE);
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    @Fork(1)
    @Warmup(iterations = 3)
    @Measurement(iterations = 10)
    public void benchmarkWithRandomPointsAndRandomQueries(Blackhole hole, RandomState state) {
        for (NonEmptyQuery q : state.queries) {
            switch (state.algo) {
            case NAIVE:
                hole.consume(Util.naiveQuery(state.points, q.min, q.max));
                break;
            case RANGE_TREE:
                hole.consume(state.rangeTree.query(q.min, q.max));
                break;
            case LAYERED_RANGE_TREE:
                hole.consume(state.layeredRangeTree.query(q.min, q.max));
                break;
            default:
                throw new AssertionError();
            }
        }
    }
}
