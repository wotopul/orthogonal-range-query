package org.wotopul.range;

import java.util.*;

/**
 * A range tree. Does not support points with equal coordinates.
 */
public class RangeTree extends AbstractRangeTree {
    /**
     * @see AbstractRangeTree#AbstractRangeTree(Set)
     */
    public RangeTree(Set<Point> points) {
        super(points);
        root = buildTree(getSortedPoints(points), 0);
    }

    private Node buildTree(Point[][] sortedPoints, int dim) {
        int size = sortedPoints[0].length;
        Node result;
        if (size == 1) {
            result = new Node(sortedPoints[0][0], dim);
        } else {
            int mid = size / 2;
            Point[][] left = new Point[dimension - dim][mid];
            Point[][] right = new Point[dimension - dim][size - mid];
            partition(sortedPoints, left, right, dim);
            double key = sortedPoints[0][mid - 1].x(dim);
            result = new Node(buildTree(left, dim), buildTree(right, dim), key);
        }
        if (dim < dimension - 1) { // otherwise leave associated tree reference as null
            // Slice of points sorted by dim-th coordinate.
            // A sorted list of points by some coordinate is passed in a recursive call
            // only if it is needed to construct a next level tree.
            Point[][] nextDimSortedPoints = Arrays.copyOfRange(
                sortedPoints, 1, sortedPoints.length);
            result.assoc = buildTree(nextDimSortedPoints, dim + 1);
        }
        return result;
    }
}
