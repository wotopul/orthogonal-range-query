package org.wotopul.range;

import java.util.*;
import java.util.stream.Collectors;

public class SearchTree {
    private final int dimension;
    private Node root;

    public SearchTree(Set<Point> points, int dimension) {
        // TODO think of edge cases
        if (points.isEmpty())
            throw new IllegalArgumentException("empty points list");
        if (points.stream().map(Point::dimension).distinct().count() > 1)
            throw new IllegalArgumentException("dimensions of points differ");
        this.dimension = dimension;
        List<Node> leaves = constructLeaves(points, dimension);
        root = buildTree(leaves, 0, leaves.size());
    }

    public List<Point> query(double min, double max) {
        if (min > max)
            return Collections.emptyList();
        Node split = findSplitNode(min, max);
        if (split.isLeaf()) {
            return Point.inRange(min, max, split.key)
                ? split.points
                : Collections.emptyList();
        }
        List<Point> result = new ArrayList<>();
        descendToMin(split, min, max, result);
        descendToMax(split, min, max, result);
        return result;
    }

    private void descendToMin(Node split, double min, double max, List<Point> result) {
        Node v = split.left;
        while (!v.isLeaf()) {
            if (min <= v.key) {
                appendAll(v.right, result);
                v = v.left;
            } else {
                v = v.right;
            }
        }
        if (Point.inRange(min, max, v.key))
            result.addAll(v.points);
    }

    private void descendToMax(Node split, double min, double max, List<Point> result) {
        Node v = split.right;
        while (!v.isLeaf()) {
            if (max > v.key) {
                appendAll(v.left, result);
                v = v.right;
            } else {
                v = v.left;
            }
        }
        if (Point.inRange(min, max, v.key))
            result.addAll(v.points);
    }

    // TODO get rid of recursion?
    private void appendAll(Node v, List<Point> result) {
        if (v == null)
            return;
        if (v.isLeaf()) {
            result.addAll(v.points);
            return;
        }
        appendAll(v.left, result);
        appendAll(v.right, result);
    }

    private Node findSplitNode(double min, double max) {
        Node v = root;
        while (!v.isLeaf() && (max <= v.key || min > v.key)) {
            v = max <= v.key ? v.left : v.right;
        }
        return v;
    }

    private List<Node> constructLeaves(Set<Point> points, int dimension) {
        SortedMap<Double, List<Point>> pointsByKey = points.stream()
            .collect(Collectors.groupingBy(p -> p.x(dimension),
                TreeMap::new, Collectors.toList()));
        return pointsByKey.values().stream().map(Node::new).collect(Collectors.toList());
    }

    private Node buildTree(List<Node> leaves, int l, int r) {
        if (r - l == 1)
            return leaves.get(l);
        int m = (l + r) / 2;
        double key = leaves.get(m - 1).key;
        Node left = buildTree(leaves, l, m);
        Node right = buildTree(leaves, m, r);
        return new Node(left, right, key);
    }

    private class Node {
        Node left, right;
        double key;
        List<Point> points;

        Node(List<Point> points) {
            left = right = null;
            key = points.get(0).x(dimension); // TODO a bug?
            this.points = points;
        }

        Node(Node left, Node right, double key) {
            assert left.key < right.key;
            this.left = left;
            this.right = right;
            this.key = key;
        }

        boolean isLeaf() {
            return points != null;
        }
    }
}
