package org.wotopul.range;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public abstract class AbstractRangeTree {
    protected final int dimension;
    protected Node root;

    /**
     * Constructs a range tree on a specified points set.
     * All points in a set must have the same dimension otherwise an
     * {@link IllegalArgumentException} is thrown
     *
     * @param points a set of points
     * @throws IllegalArgumentException if points dimension differ
     * @throws IllegalArgumentException if given point set is empty
     */
    protected AbstractRangeTree(Set<Point> points) {
        if (points.isEmpty())
            throw new IllegalArgumentException("empty points list");
        dimension = points.iterator().next().dimension();
        for (Point p : points) {
            if (p.dimension() != dimension) {
                throw new IllegalArgumentException("dimensions of points differ");
            }
        }
    }

    /**
     * Returns a list of points that lie inside of closed multidimensional
     * interval defined by its endpoints {@code min} and {@code max}.
     */
    public List<Point> query(Point min, Point max) {
        if (min.dimension() != dimension || min.dimension() != max.dimension())
            throw new IllegalArgumentException("invalid query dimensions");
        if (!min.isLessOrEqual(max))
            return Collections.emptyList();
        List<Point> result = new ArrayList<>();
        root.query(min, max, 0, result);
        return result;
    }

    /**
     * Returns a two-dimensional array {@code res[dimension][size]}
     * where {@code size} is a number of points in a tree.
     * It holds that points {@code res[i]} are sorted by their
     * {@code i}-th coordinate for every {@code i} in range {@code 0 .. dimension - 1}.
     */
    protected Point[][] getSortedPoints(Set<Point> points) {
        Point[][] result = new Point[dimension][points.size()];
        int offset = 0;
        for (Point p : points) {
            for (int i = 0; i < dimension; i++) {
                result[i][offset] = p;
            }
            offset++;
        }
        for (int i = 0; i < dimension; i++) {
            Arrays.sort(result[i], Point.comparingByDimension(i));
        }
        return result;
    }

    /**
     * Partitions sorted points list into two equal parts by {@code dim}-th coordinate.
     * Preserves sorting by each coordinate.
     */
    protected void partition(Point[][] points, Point[][] left, Point[][] right, int dim) {
        int mid = left[0].length;
        double split = points[0][mid].x(dim);
        for (int i = 0; i < dimension - dim; i++) {
            int leftOffset = 0;
            int rightOffset = 0;
            for (Point p : points[i]) {
                if (p.x(dim) < split) {
                    left[i][leftOffset++] = p;
                } else {
                    right[i][rightOffset++] = p;
                }
            }
        }
    }

    protected class Node {
        Node left, right;

        /**
         * Associated range tree that is sorted by next coordinate.
         */
        Node assoc;

        double key;

        /**
         * Null in internal nodes.
         * In leaves {@code point.x(i) == key} where {@code i}
         * is a dimension on which current tree is built.
         */
        Point point;

        Node(Point point, int dim) {
            left = right = null;
            key = point.x(dim);
            this.point = point;
        }

        Node(Node left, Node right, double key) {
            assert left.key < right.key;
            this.left = left;
            this.right = right;
            this.key = key;
        }

        boolean isLeaf() {
            return point != null;
        }

        void query(Point min, Point max, int dim, List<Point> result) {
            Node split = findSplitNode(min.x(dim), max.x(dim));
            if (split.isLeaf()) {
                if (split.point.inRange(min, max, dim))
                    result.add(split.point);
                return;
            }
            split.queryImpl(min, max, dim, result);
        }

        void queryImpl(Point min, Point max, int dim, List<Point> result) {
            descendToMin(min, max, dim, result);
            descendToMax(min, max, dim, result);
        }

        Node findSplitNode(double min, double max) {
            Node v = this;
            while (!v.isLeaf() && (max <= v.key || min > v.key)) {
                v = max <= v.key ? v.left : v.right;
            }
            return v;
        }

        void descendToMin(Point min, Point max, int dim, List<Point> result) {
            Node v = this.left;
            while (!v.isLeaf()) {
                if (min.x(dim) <= v.key) {
                    if (dim < dimension - 1) {
                        v.right.assoc.query(min, max, dim + 1, result);
                    } else if (v.right != null) {
                        v.right.addAll(result);
                    }
                    v = v.left;
                } else {
                    v = v.right;
                }
            }
            if (v.point.inRange(min, max, dim))
                result.add(v.point);
        }

        void descendToMax(Point min, Point max, int dim, List<Point> result) {
            Node v = this.right;
            while (!v.isLeaf()) {
                if (max.x(dim) > v.key) {
                    if (dim < dimension - 1) {
                        v.left.assoc.query(min, max, dim + 1, result);
                    } else if (v.right != null) {
                        v.left.addAll(result);
                    }
                    v = v.right;
                } else {
                    v = v.left;
                }
            }
            if (v.point.inRange(min, max, dim))
                result.add(v.point);
        }

        // TODO get rid of recursion?
        void addAll(List<Point> result) {
            if (isLeaf()) {
                result.add(point);
                return;
            }
            if (left != null)
                left.addAll(result);
            if (right != null)
                right.addAll(result);
        }
    }
}
