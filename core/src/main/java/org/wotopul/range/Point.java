package org.wotopul.range;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

/**
 * A multidimensional point. Instances of this class are immutable
 */
public class Point {
    private final double[] coordinates;

    public Point(double... coordinates) {
        if (coordinates.length == 0)
            throw new IllegalArgumentException("zero dimension");
        this.coordinates = Arrays.copyOf(coordinates, coordinates.length);
    }

    public int dimension() {
        return coordinates.length;
    }

    public double x(int i) {
        return coordinates[i];
    }

    public static Comparator<Point> comparingByDimension(int i) {
        return Comparator.comparing(p -> p.x(i));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        Point point = (Point) o;
        return Arrays.equals(coordinates, point.coordinates);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(coordinates);
    }

    @Override
    public String toString() {
        return Arrays.stream(coordinates)
            .mapToObj(String::valueOf)
            .collect(Collectors.joining(", ", "(", ")"));
    }

    public boolean isLessOrEqual(Point p) {
        for (int i = 0; i < dimension(); i++) {
            if (x(i) <= p.x(i))
                return true;
        }
        return false;
    }

    public boolean inRange(Point min, Point max) {
        return inRange(min, max, 0);
    }

    public boolean inRange(Point min, Point max, int startDim) {
        for (int i = startDim; i < dimension(); i++) {
            if (!inRange(min.x(i), max.x(i), x(i)))
                return false;
        }
        return true;
    }

    public static boolean inRange(double min, double max, double x) {
        return min <= x && x <= max;
    }
}
