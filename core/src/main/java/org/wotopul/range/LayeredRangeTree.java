package org.wotopul.range;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * A layered range tree. Does not support points with equal coordinates.
 */
public class LayeredRangeTree extends AbstractRangeTree {
    /**
     * @see AbstractRangeTree#AbstractRangeTree(Set)
     */
    public LayeredRangeTree(Set<Point> points) {
        super(points);
        root = buildTree(getSortedPoints(points), 0);
        ((LayeredNode) root).computeCascadingIndices(0);
    }

    private Node buildTree(Point[][] sortedPoints, int dim) {
        int size = sortedPoints[0].length;
        LayeredNode result;
        if (size == 1) {
            result = new LayeredNode(sortedPoints[0][0], dim);
        } else {
            int mid = size / 2;
            Point[][] left = new Point[dimension - dim][mid];
            Point[][] right = new Point[dimension - dim][size - mid];
            partition(sortedPoints, left, right, dim);
            double key = sortedPoints[0][mid - 1].x(dim);
            result = new LayeredNode(buildTree(left, dim), buildTree(right, dim), key);
        }
        if (dim < dimension - 2) {
            // Proceed as in regular range tree.
            // Slice of points sorted by dim-th coordinate.
            // A sorted list of points by some coordinate is passed in a recursive call
            // only if it is needed to construct a next level tree.
            Point[][] nextDimSortedPoints = Arrays.copyOfRange(
                sortedPoints, 1, sortedPoints.length);
            result.assoc = buildTree(nextDimSortedPoints, dim + 1);
        } else if (dim == dimension - 2) {
            // Allocate space for associate points array and cascading indices
            assert sortedPoints.length == 2;
            result.pointList = Arrays.copyOf(sortedPoints[1], size);
            result.leftCascadingIndices = new int[size];
            result.rightCascadingIndices = new int[size];
        }
        return result;
    }

    private class LayeredNode extends Node {
        Point[] pointList;
        int[] leftCascadingIndices;
        int[] rightCascadingIndices;

        LayeredNode(Point point, int dim) {
            super(point, dim);
        }

        LayeredNode(Node left, Node right, double key) {
            super(left, right, key);
        }

        LayeredNode left() {
            return (LayeredNode) left;
        }

        LayeredNode right() {
            return (LayeredNode) right;
        }

        LayeredNode assoc() {
            return (LayeredNode) assoc;
        }

        @Override
        void queryImpl(Point min, Point max, int dim, List<Point> result) {
            if (dim == dimension - 2) {
                query2D(min, max, result);
            } else {
                descendToMin(min, max, dim, result);
                descendToMax(min, max, dim, result);
            }
        }

        void query2D(Point min, Point max, List<Point> result) {
            int startIndex = Arrays.binarySearch(pointList, min,
                Point.comparingByDimension(dimension - 1));
            if (startIndex < 0) {
                // get "insertion point"
                startIndex = -startIndex - 1;
            }
            if (startIndex == pointList.length) {
                // The canonical subset of this vertex does not
                // contain points from query range, all points
                // have smaller values of last coordinate than min
                return;
            }
            descendToMin2D(min, max, startIndex, result);
            descendToMax2D(min, max, startIndex, result);
        }

        void descendToMin2D(Point min, Point max, int startIndex, List<Point> result) {
            LayeredNode v = this.left();
            startIndex = this.leftCascadingIndices[startIndex];
            while (!v.isLeaf() && startIndex != -1) {
                int leftIndex = v.leftCascadingIndices[startIndex];
                int rightIndex = v.rightCascadingIndices[startIndex];
                if (min.x(dimension - 2) <= v.key) {
                    v.right().addPoints1D(max, rightIndex, result);
                    v = v.left();
                    startIndex = leftIndex;
                } else {
                    v = v.right();
                    startIndex = rightIndex;
                }
            }
            if (v.isLeaf() && v.point.inRange(min, max, dimension - 2))
                result.add(v.point);
        }

        void descendToMax2D(Point min, Point max, int startIndex, List<Point> result) {
            LayeredNode v = this.right();
            startIndex = this.rightCascadingIndices[startIndex];
            while (!v.isLeaf() && startIndex != -1) {
                int leftIndex = v.leftCascadingIndices[startIndex];
                int rightIndex = v.rightCascadingIndices[startIndex];
                if (max.x(dimension - 2) > v.key) {
                    v.left().addPoints1D(max, leftIndex, result);
                    v = v.right();
                    startIndex = rightIndex;
                } else {
                    v = v.left();
                    startIndex = leftIndex;
                }
            }
            if (v.isLeaf() && v.point.inRange(min, max, dimension - 2))
                result.add(v.point);
        }

        void addPoints1D(Point max, int startIndex, List<Point> result) {
            if (startIndex == -1)
                return;
            for (int i = startIndex; i < pointList.length; i++) {
                Point p = pointList[i];
                if (p.x(dimension - 1) > max.x(dimension - 1))
                    break;
                result.add(p);
            }
        }

        void computeCascadingIndices(int dim) {
            if (dim == dimension - 2) {
                computeCascadingIndicesImpl();
            } else {
                if (dim < dimension - 2)
                    assoc().computeCascadingIndices(dim + 1);
                if (left != null)
                    left().computeCascadingIndices(dim);
                if (right != null)
                    right().computeCascadingIndices(dim);
            }
        }

        void computeCascadingIndicesImpl() {
            computeCascadingIndicesImpl(left(), leftCascadingIndices);
            computeCascadingIndicesImpl(right(), rightCascadingIndices);
        }

        void computeCascadingIndicesImpl(LayeredNode child, int[] indices) {
            if (child == null) {
                Arrays.fill(indices, -1);
                return;
            }
            int leftIndex = 0;
            for (int i = 0; i < pointList.length; i++) {
                while (leftIndex < child.pointList.length
                    && (child.pointList[leftIndex].x(dimension - 1)
                        < pointList[i].x(dimension - 1)))
                {
                    leftIndex++;
                }
                if (leftIndex == child.pointList.length) {
                    Arrays.fill(indices, i, indices.length, -1);
                    break;
                }
                indices[i] = leftIndex;
            }
            child.computeCascadingIndicesImpl();
        }
    }
}
