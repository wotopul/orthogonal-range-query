package org.wotopul.range;

import java.util.*;
import java.util.stream.Collectors;

public class Util {
    public static Point generateRandomPoint(int dimension, double maxValue) {
        double[] coordinates = new double[dimension];
        for (int j = 0; j < dimension; j++) {
            coordinates[j] = Math.random() * maxValue;
        }
        return new Point(coordinates);
    }

    /**
     * Generates a set of random points such that no two points have equal coordinates.
     */
    public static Set<Point> generateRandomPoints(int dimension, int nPoints, double maxValue) {
        Set<Point> points = new HashSet<>();
    generate:
        while (points.size() < nPoints) {
            Point newPoint = generateRandomPoint(dimension, maxValue);
            for (Point p : points) {
                for (int dim = 0; dim < dimension; dim++) {
                    if (p.x(dim) == newPoint.x(dim))
                        continue generate;
                }
            }
            points.add(newPoint);
        }
        return points;
    }

    public static List<Point> naiveQuery(Set<Point> points, Point min, Point max) {
        return points.stream()
            .filter(p -> p.inRange(min, max))
            .collect(Collectors.toList());
    }

    public static List<NonEmptyQuery> generateQueries(
        int dimension, int nQueries, double maxValue, double delta)
    {
        List<NonEmptyQuery> result = new ArrayList<>(nQueries);
        for (int i = 0; i < nQueries; i++) {
            Point min = generateRandomPoint(dimension, maxValue);
            double[] maxCoordinates = new double[dimension];
            for (int dim = 0; dim < dimension; dim++) {
                double minX = min.x(dim);
                double x = minX + Math.random() * delta;
                maxCoordinates[dim] = Math.min(x, maxValue);
            }
            Point max = new Point(maxCoordinates);
            result.add(new NonEmptyQuery(min, max));
        }
        return result;
    }

    public static class NonEmptyQuery {
        Point min, max;

        public NonEmptyQuery(Point min, Point max) {
            if (!min.isLessOrEqual(max))
                throw new IllegalArgumentException("empty range");
            this.min = min;
            this.max = max;
        }
    }
}
