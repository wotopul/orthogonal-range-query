package org.wotopul.range;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(Parameterized.class)
public class RangeTreeTest {
    @Parameterized.Parameters
    public static Object[] data() {
        return new Object[] {false, true};
    }

    @Parameterized.Parameter
    public boolean fractionalCascading;

    @Test(expected = IllegalArgumentException.class)
    public void emptyCollectionConstruction() {
        createRangeTree(Collections.emptySet(), fractionalCascading);
    }

    @Test(expected = IllegalArgumentException.class)
    public void differentDimensionPointsConstruction() {
        Set<Point> points = new HashSet<>();
        points.add(new Point(0, 0));
        points.add(new Point(1, 1, 42));
        createRangeTree(points, fractionalCascading);
    }

    @Test
    public void emptyRange() {
        AbstractRangeTree tree2D = createRangeTree(Collections.singleton(new Point(0, 0)), fractionalCascading);
        List<Point> actual = tree2D.query(new Point(0, 1), new Point(0, -1));
        testImpl(Collections.emptyList(), actual);

        AbstractRangeTree tree4D = new RangeTree(Collections.singleton(new Point(0, 0, 0, 0)));
        actual = tree4D.query(new Point(0, 1, 42, 42), new Point(0, -1, 42, 100));
        testImpl(Collections.emptyList(), actual);
    }

    @Test
    public void simple1DQuery() {
        List<Point> points = Arrays.asList(
            new Point(1),
            new Point(2),
            new Point(3),
            new Point(4),
            new Point(5)
        );
        RangeTree tree = new RangeTree(new HashSet<>(points));

        testImpl1D(points, 1, 3, tree, 1.5, 3.5);
        testImpl1D(points, 0, 1, tree, 0.5, 1.5);
        testImpl1D(points, 1, 4, tree, 1.5, 4.5);
        testImpl1D(points, 1, 4, tree, 2.0, 4.0);
        testImpl1D(points, 0, 5, tree, 1.0, 5.0);
    }

    private void testImpl1D(List<Point> points, int expectedBegin, int expectedEnd,
        RangeTree tree, double min, double max)
    {
        testImpl(points.subList(expectedBegin, expectedEnd),
            tree.query(new Point(min), new Point(max)));
    }

    @Test
    public void simple2DQuery() {
        List<Point> points = Arrays.asList(
            new Point(1, 1),
            new Point(2, 2),
            new Point(3, 3),
            new Point(4, 4),
            new Point(5, 5)
        );
        RangeTree tree = new RangeTree(new HashSet<>(points));

        testImpl2D(points, 1, 3, tree, 1.5, 1.5, 3.5, 3.5);
        testImpl2D(points, 0, 1, tree, 0.5, 0.5, 1.5, 1.5);
        testImpl2D(points, 1, 4, tree, 1.5, 1.5, 4.5, 4.5);
        testImpl2D(points, 1, 4, tree, 2.0, 2.0, 4.0, 4.0);
        testImpl2D(points, 0, 5, tree, 1.0, 1.0, 5.0, 5.0);
    }

    private void testImpl2D(List<Point> points, int expectedBegin, int expectedEnd,
        RangeTree tree, double minX, double minY, double maxX, double maxY)
    {
        testImpl(points.subList(expectedBegin, expectedEnd),
            tree.query(new Point(minX, minY), new Point(maxX, maxY)));
    }

    @Test
    public void test0() {
        Set<Point> points = new HashSet<>(Arrays.asList(
            new Point(5.13, 6.60),
            new Point(3.46, 12.25),
            new Point(15.51, 17.36)
        ));

        RangeTree tree = new RangeTree(points);

        Point min = new Point(3.19, 7.51);
        Point max = new Point(6.27, 12.40);

        testImpl(Util.naiveQuery(points, min, max), tree.query(min, max));
    }

    @Test
    public void randomTest() {
        randomTestImpl(2, 100, 100, 20);
        randomTestImpl(10, 10, 50, 1);
    }

    private void randomTestImpl(int dimension, int nPoints, int nQueries, double maxValue) {
        Set<Point> points = Util.generateRandomPoints(dimension, nPoints, maxValue);
        RangeTree tree = new RangeTree(points);

        List<Util.NonEmptyQuery> queries = Util.generateQueries(
            dimension, nQueries, maxValue, maxValue / 2);
        for (Util.NonEmptyQuery query : queries) {
            Point min = query.min;
            Point max = query.max;
            testImpl(Util.naiveQuery(points, min, max), tree.query(min, max));
        }
    }

    private void testImpl(List<Point> expected, List<Point> actual) {
        Assert.assertEquals(new HashSet<>(expected), new HashSet<>(actual));
    }

    private AbstractRangeTree createRangeTree(Set<Point> points, boolean fractionalCascading) {
        return fractionalCascading ? new LayeredRangeTree(points) : new RangeTree(points);
    }
}
