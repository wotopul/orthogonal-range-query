package org.wotopul.range;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;

public class SearchTreeTest {
    @Test(expected = IllegalArgumentException.class)
    public void emptyCollectionConstruction() {
        new SearchTree(Collections.emptySet(), 0);
    }

    @Test
    public void simple() {
        List<Point> points = Arrays.asList(
            new Point(1, 0),
            new Point(2, 0),
            new Point(3, 0),
            new Point(4, 0),
            new Point(5, 0)
        );
        SearchTree tree = new SearchTree(new HashSet<>(points), 0);

        testImpl(points.subList(1, 3), tree.query(1.5, 3.5));
        testImpl(points.subList(0, 1), tree.query(0.5, 1.5));
        testImpl(points.subList(1, 4), tree.query(1.5, 4.5));
        testImpl(points.subList(1, 4), tree.query(2.0, 4.0));
        testImpl(points.subList(0, 5), tree.query(1.0, 5.0));
    }

    @Test
    public void emptyRange() {
        List<Point> points = Arrays.asList(
            new Point(1, 0, 1),
            new Point(2, 0, 1),
            new Point(2, 0, 1)
        );
        SearchTree tree = new SearchTree(new HashSet<>(points), 0);
        testImpl(Collections.emptyList(), tree.query(42, 0));
    }

    @Test
    public void equalFirstCoordinates() {
        List<Point> points = Arrays.asList(
            new Point(42, -2),
            new Point(42, -1),
            new Point(42, -0),
            new Point(42, +1),
            new Point(42, +2)
        );
        SearchTree tree = new SearchTree(new HashSet<>(points), 0);

        testImpl(points, tree.query(0, 100));
        testImpl(points, tree.query(42, 42));
        testImpl(Collections.emptyList(), tree.query(0, 1));
    }

    @Test
    public void randomTest() {
        final int nPoints = 100;
        final double maxValue = 10;
        List<Point> points = new ArrayList<>();
        for (int i = 0; i < nPoints; i++) {
            points.add(new Point(
                randomValue(maxValue),
                randomValue(maxValue),
                randomValue(maxValue)));
        }
        SearchTree tree = new SearchTree(new HashSet<>(points), 1);

        final int nQueries = 100;
        for (int i = 0; i < nQueries; i++) {
            double min = randomValue(maxValue);
            double max = randomValue(maxValue);
            testImpl(naiveQuery(points, min, max), tree.query(min, max));
        }
    }

    private double randomValue(double maxValue) {
        return Math.random() * maxValue;
    }

    private List<Point> naiveQuery(List<Point> points, double min, double max) {
        return points.stream()
            .filter(p -> Point.inRange(min, max, p.x(1)))
            .collect(Collectors.toList());
    }

    private void testImpl(List<Point> expected, List<Point> actual) {
        Assert.assertEquals(new HashSet<>(expected), new HashSet<>(actual));
    }
}