# Orthogonal range query #

[ Запрос ](https://bitbucket.org/wotopul/orthogonal-range-query/src/72bec7db279f3bb9391f9e52191ac2b8f78b9f71/core/src/main/java/org/wotopul/range/AbstractRangeTree.java?at=master&fileviewer=file-view-default#AbstractRangeTree.java-37) задается двумя точками в R^d — нижней и верхней границей интервала.

## [ RangeTree ](https://bitbucket.org/wotopul/orthogonal-range-query/src/72bec7db279f3bb9391f9e52191ac2b8f78b9f71/core/src/main/java/org/wotopul/range/RangeTree.java?at=master&fileviewer=file-view-default) — статическая задача в R^d c O(log^d n) + k на запрос. ##

При n = 50000 производительнее наивной версии ( O(n * d) ) в 1.5 * 10^3 раз.

## [ LayeredRange ](https://bitbucket.org/wotopul/orthogonal-range-query/src/72bec7db279f3bb9391f9e52191ac2b8f78b9f71/core/src/main/java/org/wotopul/range/LayeredRangeTree.java?at=master&fileviewer=file-view-default) — статическая задача в R^d c O(log^(d-1) n) + k на запрос. ##

![range.png](https://bitbucket.org/repo/RbMzaM/images/3435850742-range.png)